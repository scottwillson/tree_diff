require_relative '../lib/tree_diff'
require_relative 'helper'
require 'minitest/autorun'

class TreeDiffTest < Minitest::Test
  class DeliveryDiff < TreeDiff
    observe :at, :quantity
  end

  class ConditionalDiff < TreeDiff
    observe :number, line_items: [:description, :price_usd_cents, item_categories: [:short_name, :description]]

    condition [:line_items, :item_categories, :short_name] do |category|
      category.description == 'foo'
    end
  end

  class OrderDiff < TreeDiff
    observe :number, line_items: [:description, :price_usd_cents, item_categories: [:description]]
  end

  class OrderVehicleDiff < TreeDiff
    observe trucks: [:number], orders_vehicles: [{vehicle: [:id]}]
  end

  class OmittingArraysDiff < TreeDiff
    observe :number, orders_vehicles: {vehicle: :id}
  end

  class InvolvingOrderDetailDiff < TreeDiff
    observe :number, detail: [:integration_id, :external_id]
  end

  class TableDiff < TreeDiff
    observe wood_finish: [:color, :cost, :applied_at, :cost_mapping,
                          blemishes: [:severity]]
  end

  class EmptyDiff < TreeDiff
  end

  def test_simple_one_table
    delivery = Delivery.create!(at: '12:30', quantity: 50)

    diff = DeliveryDiff.new(delivery)

    assert_empty diff.changes

    delivery.update!(at: '1:30')

    assert_equal [[:at]], diff.changed_paths
    assert_equal [{path: [:at], old: Time.gm(2000, 1, 1, 12, 30), new: Time.gm(2000, 1, 1, 1, 30)}], diff.changes

    assert_equal true, diff.changed?([:at])
    assert_equal true, diff.changed?(:at)
    assert_equal false, diff.changed?([:quantity])
    assert_equal false, diff.changed?(:quantity)

    change = diff.changes_at(:at)
    assert_kind_of TreeDiff::Change, change
    assert_equal [:at], change.path
    assert_equal Time.gm(2000, 1, 1, 12, 30), change.old
    assert_equal Time.gm(2000, 1, 1, 1, 30), change.new

    assert_nil diff.changes_at(:quantity)
  end

  def test_nil_associations
    order = Order.create!(number: 'XYZ123')

    diff = InvolvingOrderDetailDiff.new(order)
    assert_empty diff.changes

    order.update!(detail: ExtraOrderDetail.new(integration_id: '123', external_id: '321'))

    assert_equal [{:path=>[:detail, :integration_id], :old=>nil, :new=>"123"},
                  {:path=>[:detail, :external_id], :old=>nil, :new=>"321"}], diff.changes
  end

  def test_big_tree
    foo_category = ItemCategory.new(description: 'foo')
    bar_category = ItemCategory.new(description: 'bar')

    thing = LineItem.new(description: 'thing', price_usd_cents: 1234, item_categories: [foo_category, bar_category])
    other_thing = LineItem.new(description: 'other thing', price_usd_cents: 5678)
    order = Order.create!(number: 'XYZ123', line_items: [thing, other_thing])

    diff = OrderDiff.new(order)

    assert_empty diff.changes

    order.update!(line_items_attributes: [{id: thing.id, description: 'thing', price_usd_cents: 1234, _destroy: true},
                                          {id: other_thing.id, description: 'other thing', price_usd_cents: 5678,
                                           item_categories_attributes: [{description: 'foo'}]},
                                          {description: 'new thing', price_usd_cents: 1357}])

    assert_equal [[:line_items, :description],
                  [:line_items, :price_usd_cents],
                  [:line_items, :item_categories, :description]], diff.changed_paths

    assert_equal [{path: [:line_items, :description], old: ["thing", "other thing"], new: ["other thing", "new thing"]},
                  {path: [:line_items, :price_usd_cents], old: [1234, 5678], new: [5678, 1357]},
                  {path: [:line_items, :item_categories, :description], old: ['foo', 'bar'], new: ['foo']}], diff.changes
  end

  def test_polymorphic_path
    truck = Truck.new(number: 123, capacity: 55)
    # other_truck = Truck.new(number: 456, capacity: 25)
    order = Order.create!(number: 'XYZ123', trucks: [truck])

    diff = OrderVehicleDiff.new(order)

    assert_empty diff.changes

    order.update!(trucks: [])

    assert_equal [[:trucks, :number],
                  [:orders_vehicles, :vehicle, :id]], diff.changed_paths

    changes = diff.changes_as_objects
    assert_equal [123], changes.first.old
    assert_equal [], changes.first.new

    assert_kind_of Integer, changes.last.old.first
    assert_equal [], changes.last.new
  end

  def test_omitting_arrays_of_child_attributes
    truck = Truck.new(number: 123, capacity: 55)
    order = Order.create!(number: 'XYZ123', trucks: [truck])

    diff = OmittingArraysDiff.new(order)

    assert_empty diff.changes

    order.update!(number: 'ZYX987')

    assert_equal [{:path => [:number], :old => "XYZ123", :new => "ZYX987"}], diff.changes
  end

  def test_poro_tree
    blemish = Blemish.new(severity: 'super_bad')
    wood_finish = WoodFinish.new(color: 'mahogany',
                                 cost: 500.0,
                                 applied_at: Time.now.utc,
                                 blemishes: [blemish],
                                 cost_mapping: {mahogany: 500.0,
                                                pine: 250.0,
                                                plywood: 60.0})

    table = Table.new(wood_finish: wood_finish)

    diff = TableDiff.new(table)

    wood_finish = table.wood_finish
    wood_finish.blemishes.first.severity = 'not_that_bad'
    wood_finish.cost_mapping[:pine] = 200.0

    assert_equal [{:path => [:wood_finish, :cost_mapping],
                   :old => {mahogany: 500.0, pine: 250.0, plywood: 60.0},
                   :new => {mahogany: 500.0, pine: 200.0, plywood: 60.0}},
                  {:path => [:wood_finish, :blemishes, :severity],
                   :old => ["super_bad"],
                   :new => ["not_that_bad"]}], diff.changes
  end

  def test_honors_conditions
    foo_category = ItemCategory.new(description: 'foo', short_name: 'Cool category')
    bar_category = ItemCategory.new(description: 'bar', short_name: 'Not very cool category')

    line_item = LineItem.new(item_categories: [foo_category, bar_category])
    order = Order.create!(line_items: [line_item])

    diff = ConditionalDiff.new(order)
    assert_empty diff.changes

    order.update!(line_items_attributes: [{id: line_item.id,
                                           item_categories_attributes: [{id: foo_category.id, description: 'foo',
                                                                         short_name: 'Cool category'},
                                                                         {id: bar_category.id, description: 'bar',
                                                                          short_name: 'another value'}]}])

    assert_equal false, diff.changed_paths.include?([:line_items, :item_categories, :short_name])
  end

  def test_abstract_class
    e = assert_raises RuntimeError do
      TreeDiff.new(Object.new)
    end

    assert_includes e.message, 'is an abstract'
  end

  def test_requires_observations
    e = assert_raises RuntimeError do
      EmptyDiff.new(Object.new)
    end

    assert_includes e.message, 'some observations first'
  end
end
