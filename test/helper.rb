require 'active_record'

ActiveRecord::Base.establish_connection(
  adapter: 'sqlite3',
  database: ':memory:'
)

ActiveRecord::Schema.define do
  create_table :orders, force: true do |t|
    t.string :number
  end

  create_table :extra_order_details, force: true do |t|
    t.references :order
    t.string :integration_id
    t.string :external_id
  end

  create_table :line_items, force: true do |t|
    t.string :description
    t.integer :price_usd_cents
    t.belongs_to :order, index: true
  end

  create_table :item_categories, force: true do |t|
    t.string :short_name
    t.string :description
    t.integer :ordering
    t.belongs_to :line_item, index: true
  end

  create_table :deliveries, force: true do |t|
    t.time :at
    t.integer :quantity
  end

  create_table :orders_vehicles, force: true do |t|
    t.datetime :created_at
    t.belongs_to :order, index: true
    t.belongs_to :vehicle, polymorphic: true
  end

  create_table :trucks, force: true do |t|
    t.integer :number
    t.integer :capacity
  end

  create_table :delivery_bikes, force: true do |t|
    t.integer :max_speed
    t.string :maintenance_status
  end
end

class Order < ActiveRecord::Base
  has_many :line_items, inverse_of: :order
  has_many :orders_vehicles
  has_one :detail, class_name: "ExtraOrderDetail"

  has_many :trucks, through: :orders_vehicles, source: :vehicle, source_type: "Vehicle", class_name: "Truck"

  accepts_nested_attributes_for :line_items, allow_destroy: true
end

class ExtraOrderDetail < ActiveRecord::Base
  belongs_to :order
end

class LineItem < ActiveRecord::Base
  belongs_to :order, inverse_of: :line_items
  has_many :item_categories

  accepts_nested_attributes_for :item_categories, allow_destroy: true
end

class ItemCategory < ActiveRecord::Base
  belongs_to :line_item, inverse_of: :item_categories
end

class Delivery < ActiveRecord::Base
end

class OrdersVehicle < ActiveRecord::Base
  belongs_to :order, inverse_of: :orders_vehicles
  belongs_to :vehicle, polymorphic: true
end

class Truck < ActiveRecord::Base
end

class DeliveryBike < ActiveRecord::Base
end

module HashInitialization
  def initialize(hash)
    hash.each do |k, v|
      public_send("#{k}=", v)
    end
  end
end

class Table
  include HashInitialization
  attr_accessor :wood_finish
end

class WoodFinish
  include HashInitialization
  attr_accessor :color, :cost, :applied_at, :blemishes, :cost_mapping
end

class Blemish
  include HashInitialization
  attr_accessor :severity
end
