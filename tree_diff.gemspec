Gem::Specification.new do |s|
  s.name        = 'tree_diff'
  s.version     = '1.2.0'
  s.date        = '2019-08-23'
  s.summary     = "Observe attribute changes in big complex object trees, like a generic & standalone ActiveModel::Dirty"
  s.description = "Given a tree of relationships in a similar format to strong params, analyzes attribute changes by " \
                  "call chain. Call just before and after you make a change. Completely ORM and framework agnostic."
  s.authors     = ["Michael Nelson"]
  s.email       = 'michael@nelsonware.com'
  s.files       = `git ls-files`.split("\n")
  s.test_files  = `git ls-files -- test/*`.split("\n")
  s.homepage    = 'https://gitlab.com/mcnelson/tree_diff'
  s.license     = 'MIT'

  # s.require_paths = ["lib"]
  s.required_ruby_version = '>= 2.5.1' # TODO

  s.add_development_dependency "minitest", "~> 5.11"
  s.add_development_dependency "m", "~> 1.5"
  s.add_development_dependency "sqlite3", "~> 1.4"
  s.add_development_dependency "activerecord", "~> 5.2"
end
